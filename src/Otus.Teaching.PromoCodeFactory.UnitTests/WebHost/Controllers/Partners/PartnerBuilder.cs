﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private Partner _partner = new Partner();

        public PartnerBuilder WithBaseProperties()
        {
            _partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };

            return this;
        }

        public PartnerBuilder WithActive(bool active = true)
        {
            _partner.IsActive = active;
            return this;
        }

        public PartnerBuilder WithNotActiveLimit()
        {
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("4418D0AD-C0C7-40E8-A280-3D9981C0D082"),
                        CreateDate = new DateTime(2023, 01, 9),
                        EndDate = DateTime.Now.AddDays(-1),
                        CancelDate = DateTime.Now.AddDays(-1),
                        Limit = 100
                    }
            };

            return this;
        }

        public PartnerBuilder WithActiveLimit()
        {
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("A2998FA2-ED6A-44E8-A715-77F517B66B9A"),
                        CreateDate = new DateTime(2023, 02, 22),
                        EndDate = DateTime.Now.AddDays(1),
                        Limit = 100
                    }
            };
            _partner.NumberIssuedPromoCodes = 50;

            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int number)
        {
            _partner.NumberIssuedPromoCodes = number;

            return this;
        }

        public Partner Create()
        {
            return _partner;
        }

        //public static Partner WithNegativeLimit(this Partner partner)
        //{
        //    partner.PartnerLimits = new List<PartnerPromoCodeLimit>
        //    {
        //        new PartnerPromoCodeLimit()
        //            {
        //                Id = Guid.Parse("58FE5E2C-3184-426C-AB94-30ADA929EC41"),
        //                CreateDate = new DateTime(2023, 02, 22),
        //                EndDate = new DateTime(2023, 12, 9),
        //                CancelDate = new DateTime(2023, 03, 22),
        //                Limit = -100
        //            }
        //    };
        //    return partner;
        //}

    }
}
