﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
               
        /// <summary>
        /// Если партнер не найден, выдается ошибка 404
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arrange
            var autoFixture = new Fixture();

            var partherId = Guid.Parse("A05678CD-07D9-4C79-810F-A539FAE13F36");
            var partnerPromocodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();     
            Partner partner = null;

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partherId))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partherId, partnerPromocodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, выдается ошибка 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromocodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithActive(false)
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromocodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то обнуляется количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит не закончился
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_NumberIssuedPromoCodesShouldBeZero()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromocodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithNumberIssuedPromoCodes(100)
                .WithActiveLimit()
                .Create();
 
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromocodeLimitRequest);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, но прошлый еще не закончился, то количество промокодов, которые партнер выдал NumberIssuedPromoCodes, не обнуляется
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerPreviousLimitIsEnd_NumberIssuedPromoCodesShouldNotBeZero()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromocodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithNumberIssuedPromoCodes(100)
                .WithNotActiveLimit()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromocodeLimitRequest);

            //Assert
            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }

        /// <summary>
        /// При установке лимита отключается предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_CancelDateHasValue()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromocodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithNotActiveLimit()
                .Create();

            var partnerLimit = partner.PartnerLimits.First();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromocodeLimitRequest);

            //Assert
            partnerLimit.CancelDate.Should().HaveValue();

        }

        /// <summary>
        /// Устанавливаемый лимит больше нуля
        /// </summary>
        [Theory]
        [InlineData(0)]
        [InlineData(-100)]
        public async void SetPartnerPromoCodeLimitAsync_PartnerLimitLessOrEqualZero_ReturnsBadRequest(int limit)
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromocodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, limit)
                .Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromocodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Устанавливаемый лимит успешно сохранен в базу данных
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerLimitIsValid_SavedSuccessfully()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromocodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, 5)
                .Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromocodeLimitRequest);

            //Assert
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(partner), Times.Once);
        }

    }
}